FROM golang:1.15.2-alpine3.12
ARG VER=2.7.0
ARG user=ibuser

RUN addgroup -S ${user} && adduser -S -G ${user} ${user}
RUN apk add --no-cache git curl jq bash

RUN curl -sfL https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s v$VER
ENV GOPATH /go/bin/

USER ${user}
ENTRYPOINT []
